from flask import Flask, request, jsonify
from flask_cors import CORS, cross_origin
import jwt
import datetime

app = Flask(__name__)
CORS(app)


def encodeAuthToken(user_id, groups=[]):
    try:
        trader = True if 'trader' in groups else False
        security = True if 'security' in groups else False

        payload = {
            'exp': datetime.datetime.utcnow() + datetime.timedelta(days=1, seconds=60),
            'iat': datetime.datetime.utcnow(),
            'sub': user_id,
            'trader': trader,
            'security': security
        }
        token = jwt.encode(payload, 'deal', algorithm='HS256')
        return token
    except Exception as e:
        print(e)
        return e


@app.route('/auth/login', methods=['POST'])
@cross_origin()
def loginAndGenerateToken():
    valid_user_1 = {'username': "security", 'password': '123'}
    valid_user_2 = {'username': 'trader', 'password': '321'}

    print(request)
    req_json = request.get_json()
    print(req_json)
    username = req_json['username']
    print(username)
    password = req_json['password']
    print(password)

    try:
        if username == valid_user_1['username'] and password == valid_user_1['password']:
            token = encodeAuthToken(1, ['security'])
            role = 'security'

        if username == valid_user_2['username'] and password == valid_user_2['password']:
            token = encodeAuthToken(2, ['trader'])
            role = 'trader'


        print(token)
        return jsonify(result={
            'status': 'success',
            'auth_token': str(token)[2:-1],
            'role': role
        })
    except Exception as e:
        return jsonify({
            'status': 'Failure',
            'error': e
        })


@app.route('/test_get_with_validation', methods=['GET'])
@cross_origin()
def testGetWithValidation():
    auth_header = request.headers.get('Authorization')
    if auth_header:
        token = auth_header.split(" ")[1]  # Parses out the "Bearer" portion
    else:
        token = ''

    if token:
        decoded = decodeAuthToken(token)

        if not isinstance(decoded, str):
            if decoded['trader']:
                return createJson('You Are a Real trader!!', 'trader', False)
            elif decoded['security']:
                return createJson('You Are A Real Security!', 'security', False)
        else:
            return createJson('Ooops, validation messed up: ' + decoded, 'none', False), 401

    return createJson('', '', True)


def createJson(message, role, error):
    json = {"status": message, "role": role, "error": error}
    return jsonify(json)


def decodeAuthToken(token):
    try:
        payload = jwt.decode(token, 'deal', algorithms='HS256')
        return payload
    except jwt.ExpiredSignatureError:
        return 'Signature expired. Login please'
    except jwt.InvalidTokenError:
        return 'Nice try, invalid token. Login please'


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
